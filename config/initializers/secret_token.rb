# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = 'd7e8c12c4ce1cdc3803ce6257db3e705a5352a31779c68bcc7a40d41ca40ce4ab4cc3df7cb18ee6223dfdc96e86330c07074a2f13b267d7346719371bae8f58a'
